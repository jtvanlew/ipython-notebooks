Welcome to the CFD Online Lesson Repository

Spanish version now available [here](https://bitbucket.org/franktoffel/cfd-python-class-es).
Thanks to F.J. Navarro-Brull and [CAChemE.org](http://www.cacheme.org/)

Lessons
-------

* [Quick Python Intro](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/00%2520-%2520Quick%2520Python%2520Intro.ipynb)
* [Step 1](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/01%2520-%2520Step%25201.ipynb)
* [Step 2](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/02%2520-%2520Step%25202.ipynb)
* [CFL Condition](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/03%2520-%2520CFL%2520Condition.ipynb)
* [Step 3](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/04%2520-%2520Step%25203.ipynb)
* [Step 4](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/05%2520-%2520Step%25204.ipynb)
* [Array Operations with NumPy](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/06%2520-%2520Array%2520Operations%2520with%2520NumPy.ipynb)
* [Step 5](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/07%2520-%2520Step%25205.ipynb)
* [Step 6](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/08%2520-%2520Step%25206.ipynb)
* [Step 7](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/09%2520-%2520Step%25207.ipynb)
* [Step 8](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/10%2520-%2520Step%25208.ipynb)
* [Defining Function in Python](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/11%2520-%2520Defining%2520Function%2520in%2520Python.ipynb)
* [Step 9](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/12%2520-%2520Step%25209.ipynb)
* [Step 10](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/13%2520-%2520Step%252010.ipynb)
* [Optimizing Loops with Numba](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/14%2520-%2520Optimizing%2520Loops%2520with%2520Numba.ipynb)
* [Step 11](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/15%2520-%2520Step%252011.ipynb)
* [Step 12](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/16%2520-%2520Step%252012.ipynb)
* [NumbaPro](http://nbviewer.ipython.org/urls/bitbucket.org/cfdpython/cfd-python-class/raw/master/lessons/17%2520-%2520NumbaPro.ipynb)
